# plexDong 🍆
Takes the current play count from a [Tautulli](https://github.com/Tautulli/Tautulli) database file and generates an ASCII Dong 🍆 based on the amount.

## ⚠ NOTICE ⚠

This is intended as a joke, please do not take it seriously.

This is intended for personal use only, so it is not very secure and may result in your database being exposed if left unattended. In the future, I will implement more security measures (if I feel like it).

## Installation
- You must have [Tautulli](https://github.com/Tautulli/Tautulli) installed and fully configured.
- You must have a webserver running to make this work, I used Apache. 
- Clone this repository in the correct HTML documents folder (i.e. `/var/www/html` for Apache).
- This requires a [Tautulli](https://github.com/Tautulli/Tautulli) database file to be specified in `/lib/ConnectDB.class.php` **MAKING A BACKUP OF THIS FILE IS RECOMMENDED**.
- Go crazy.

## <a href="https://plexdong.herokuapp.com">Example</a>

<a href="#">![Example](/assets/output.png)</a>
